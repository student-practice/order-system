package ru.mera.services;

import ru.mera.dtos.DishDto;
import ru.mera.models.Dish;
import ru.mera.models.Ingredient;

import java.util.List;

public interface DishService {
    Dish add(DishDto dishDto);
    Dish get(Long id);
    Dish update(Long id, DishDto dishDto);
    void delete(Long id);
    List<Dish> getAll();
}
