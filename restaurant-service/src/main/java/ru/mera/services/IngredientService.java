package ru.mera.services;

import ru.mera.models.Ingredient;

import java.util.List;

public interface IngredientService {
    Ingredient add(Ingredient ingredient);
    Ingredient get(Long id);
    Ingredient update(Long id, Ingredient ingredient);
    List<Ingredient> getAll();
    void delete(Long id);
}
