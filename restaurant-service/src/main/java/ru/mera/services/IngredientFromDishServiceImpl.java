package ru.mera.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mera.models.Dish;
import ru.mera.models.Ingredient;
import ru.mera.repositories.DishRepository;

import java.util.List;

@Service
public class IngredientFromDishServiceImpl implements IngredientFromDishService{

    @Autowired
    private DishRepository dishRepository;

    @Override
    public List<Ingredient> getIngredients(Dish dish) {
        Dish dishWithIngredients = dishRepository.findDishByName(dish.getName());
        return dishWithIngredients.getIngredients();
    }
}
