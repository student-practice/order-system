package ru.mera.services;

import ru.mera.dtos.OrderResponse;

public interface MessageBrokerSender {
    void sendReply(OrderResponse response);
}