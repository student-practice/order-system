package ru.mera.services;

import ru.mera.dtos.OrderDishDto;
import ru.mera.dtos.OrderResponse;

public interface OrderValidateService {
    OrderResponse validate(OrderDishDto order);
}
