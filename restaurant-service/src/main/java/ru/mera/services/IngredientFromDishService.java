package ru.mera.services;

import ru.mera.models.Dish;
import ru.mera.models.Ingredient;

import java.util.List;

public interface IngredientFromDishService {
    List<Ingredient> getIngredients(Dish dish);
}
