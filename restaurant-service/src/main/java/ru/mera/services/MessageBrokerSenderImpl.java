package ru.mera.services;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mera.config.BrokerConfiguration;
import ru.mera.dtos.OrderResponse;

@Component
public class MessageBrokerSenderImpl implements MessageBrokerSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void sendReply(OrderResponse response) {
        rabbitTemplate.convertAndSend(BrokerConfiguration.DIRECT_EXCHANGE_NAME,
                BrokerConfiguration.RESTAURANT_ROUTING_KEY, response);
    }
}