package ru.mera.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mera.models.Dish;

@Repository
public interface DishRepository extends JpaRepository<Dish, Long> {
    Dish findDishByName(String name);
}
