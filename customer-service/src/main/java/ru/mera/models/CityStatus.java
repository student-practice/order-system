package ru.mera.models;

public enum CityStatus {
    SERVED,
    NOT_SERVED
}
