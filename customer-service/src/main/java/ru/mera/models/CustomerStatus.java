package ru.mera.models;

public enum CustomerStatus {
    CUSTOMER_VALIDATED,
    CUSTOMER_NOT_VALIDATED
}
