package ru.mera.services;

import ru.mera.dtos.CustomerDto;
import ru.mera.dtos.LoginDto;
import ru.mera.dtos.TokenDto;
import ru.mera.models.Customer;

public interface CustomersService {
    Customer add(CustomerDto customerDto);
    Customer get(Long id);
    Customer update(Long id, CustomerDto customerDto);
    void delete(Long id);

    TokenDto login(LoginDto loginDto);
    TokenDto register(CustomerDto customerDto);

    boolean checkBlockList(Long id);
    boolean checkBlockList(String jwt);

}
