package ru.mera.services;

import ru.mera.dtos.CityDto;
import ru.mera.models.City;

import java.util.List;

public interface CitiesService {
    City add(CityDto cityDto);
    City get(Long id);
    City update(Long id, CityDto cityDto);
    void delete(Long id);

    List<City> getAll();
}
