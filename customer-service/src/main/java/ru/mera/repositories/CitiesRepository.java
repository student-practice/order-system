package ru.mera.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mera.models.City;

@Repository
public interface CitiesRepository extends JpaRepository<City, Long> {
}
