package ru.mera.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mera.models.Dish;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderRestaurantResponse implements Serializable {

    @JsonProperty("orderId")
    private Long orderId;

    @JsonProperty("isValidate")
    private boolean isValidate;
}