package ru.mera.services;

import ru.mera.dto.CartDto;
import ru.mera.dto.DishDto;
import ru.mera.dto.NewDishDto;
import ru.mera.models.Cart;

public interface CartService {

    Cart getCartById(Long id);
    Cart addDishToCart(NewDishDto newDishDto);
    Cart deleteItemFromCart(DishDto dishDto);
    Cart deleteDishFromCart(DishDto dishDto);
    Cart deleteCart(CartDto cartDto);
}
