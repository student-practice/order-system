package ru.mera.services;

import ru.mera.dtos.OrderCustomerDto;
import ru.mera.dtos.OrderDishDto;

public interface MessageBrokerSender {
    void sendMessageToRestaurant(OrderDishDto order);

    void sendMessageToCustomer(OrderCustomerDto order);
}