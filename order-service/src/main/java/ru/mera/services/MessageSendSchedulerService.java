package ru.mera.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.mera.dtos.OrderCustomerDto;
import ru.mera.dtos.OrderDishDto;
import ru.mera.models.Order;
import ru.mera.models.OutBox;
import ru.mera.repositories.OrderRepository;
import ru.mera.repositories.OutBoxRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessageSendSchedulerService {

    @Autowired
    OutBoxRepository outBoxRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    MessageBrokerSender sender;

    @Scheduled(cron = "0 0/1 * * * ?")
    private void sendAndDeleteAllMessages() {
        List<OutBox> orders = outBoxRepository.findAll();
        for (OutBox outboxOrder :
                orders) {
            Order order = orderRepository.findById(outboxOrder.getOrderId()).orElseThrow(IllegalArgumentException::new);
            sender.sendMessageToCustomer(new OrderCustomerDto(order.getId(), order.getUserId()));
            sender.sendMessageToRestaurant(
                    new OrderDishDto(order.getId(),
                            order.getCartItems().stream()
                                    .map(s -> s.getDish().getId())
                                    .collect(Collectors.toList())));
            outBoxRepository.delete(outboxOrder);
        }
    }
}