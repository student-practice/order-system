package ru.mera.services;

public interface NextSequenceService {
    Long getNextSequence(String sequenceName);
}
