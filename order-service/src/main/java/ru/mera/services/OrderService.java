package ru.mera.services;

import ru.mera.dto.OrderDto;
import ru.mera.models.Order;

import java.util.List;

public interface OrderService {

    List<Order> getAllUserOrders(Long id);
    Order doOrder(OrderDto orderDto);
    Order getById(Long id);
    Order update(Order order);
}
