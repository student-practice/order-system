package ru.mera.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mera.enums.ResponseStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerValidate {

    private ResponseStatus status;
}