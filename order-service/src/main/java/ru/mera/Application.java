package ru.mera;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.mera.repositories.OrderRepository;
import ru.mera.services.NextSequenceService;


@SpringBootApplication
@EnableAsync
@EnableScheduling
public class Application implements CommandLineRunner {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    NextSequenceService nextSequenceService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        System.out.println(orderRepository.findAllReceived());
    }
}