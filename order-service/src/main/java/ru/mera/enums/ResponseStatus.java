package ru.mera.enums;

public enum ResponseStatus {
    WAITING, RECEIVED, REJECTED
}