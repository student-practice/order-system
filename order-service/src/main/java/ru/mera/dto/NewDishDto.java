package ru.mera.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NewDishDto {

    private Long userId;
    private Long dishId;
    private String name;
    private String description;
}
