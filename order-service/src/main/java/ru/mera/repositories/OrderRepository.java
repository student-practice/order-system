package ru.mera.repositories;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import ru.mera.models.Dish;
import ru.mera.models.Order;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends MongoRepository<Order, Long> {

    List<Order> findAllByUserId(Long id);
    @Query(value = "{'status': 'RECEIVED'}")
    List<Order> findAllReceived();

}
