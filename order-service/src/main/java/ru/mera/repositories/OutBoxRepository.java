package ru.mera.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.mera.models.OutBox;


public interface OutBoxRepository extends MongoRepository<OutBox, Long> {
}