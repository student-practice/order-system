package ru.mera.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.mera.models.Cart;

import java.util.Optional;

public interface CartRepository extends MongoRepository<Cart, Long> {

}
